	var deusSliderMethods = {
		init: function(options) {
			if(this.get(0) === undefined){
				console.log('Deus Slider: DOM element does not exist on the page');
				return false;
			}
			if(options === undefined) options = 1000;
			var interval = 1000;
			if(jQuery.isNumeric(options)) interval = options;
			if(options.interval != null) interval = options.interval;
			//ul_class
			/*VARS*/
			var _this = this;
			var slideTimer = null;//defined in constructor
			var slide_num = 0;
			var slide_prev = -1;
			var slide_next = 1;
			var slide_max = -1;
			var slide_to_fade = null;//defined in constructor
			var action_timer = jQuery.now();
			var delta_action_time = 1000;
			var random_seed = Math.floor((Math.random() * 1000000) + 1);
			if(interval < 1000) delta_action_time = interval;

			/*METHODS*/
			this.get(0).to_slide = function(num){
				if(jQuery.now() - action_timer < delta_action_time ) return false;
				num = num * 1;
				if(slide_num == num) return false;
				if(num > slide_max) num = slide_max;
				if(num < 0 ) num = 0;
				var id = num * 1;
				slide_next = id;
				_this.get(0).next_slide();
				clearInterval(slideTimer);
				slideTimer = setInterval( _this.get(0).next_slide, interval );
				return false;
			}
			
			this.get(0).next_slide = function(){
				if(jQuery.now() - action_timer < delta_action_time ) return false; action_timer = jQuery.now();
				slide_to_fade = null;
				_this.find("> li").each(function(index){
					if(index == slide_next)
					{
						jQuery(this).css('zIndex', '20');
						if(slide_to_fade == null) slide_to_fade = jQuery(this);
					}

					if(index == slide_num) {
						jQuery(this).css('zIndex', '15');
						jQuery(this).fadeOut();
					}
					else jQuery(this).css('opacity', '0');
					if(slide_max > 1) // больше двух слайдов
					{
						num1 = slide_next - 1;
						num2 = slide_next + 1;
						if(num1 < 0) num1 = slide_max;
						if(num2 > slide_max) num2 = 0;
						var selector = _this.attr('id');
						if(selector == undefined)
							selector = '.' + _this.attr('class').replace(' ', '.');
						else
							selector = '#' + selector + '.' + _this.attr('class').replace(' ', '.');
						if(index == num1)
						{
							jQuery('#pseudo_before_style' + random_seed).remove();
							jQuery('head').append('<style id="pseudo_before_style' + random_seed + '" type="text/css"> ul' + selector + ':before { background: '+jQuery(this).css('backgroundImage')+'; } </style>');
						}
						if(index == num2)
						{
							jQuery('#pseudo_after_style' + random_seed).remove();
							jQuery('head').append('<style id="pseudo_after_style' + random_seed + '" type="text/css"> ul' + selector + ':after { background: '+jQuery(this).css('backgroundImage')+'; } </style>');
						}
					}
				});
				_this.find("> li.deus_slide_nav, li.deus_slide_buttons").css('opacity', '1');
				_this.find("> li.deus_slide_nav, li.deus_slide_buttons").css('zIndex', '25');
				
				slide_num = slide_next;
				if(slide_max == slide_next) slide_next = 0;
				else slide_next = slide_next + 1;

				slide_to_fade.fadeTo( "slow", 1 , function(){
					_this.find("> li.deus_slide_nav a").each(function(index){
						if(index == slide_num) jQuery(this).addClass('deus_slide_active');
						else jQuery(this).removeClass('deus_slide_active');
					})
				});
				if(options.onSlide !== undefined) options.onSlide({slideMax: slide_max, slideNum: slide_num});
			}
			
			this.get(0).prev_slide = function(){
				if(jQuery.now() - action_timer < delta_action_time ) return false; action_timer = jQuery.now();
				slide_prev = slide_next - 2;
				if(slide_prev == -1) slide_prev = slide_max;
				if(slide_prev == -2) slide_prev = slide_max - 1;
				slide_to_fade = null;
				_this.find("> li").each(function(index){
					if(index == slide_prev)
					{
						jQuery(this).css('zIndex', '20');
						if(slide_to_fade == null) slide_to_fade = jQuery(this);
					}
					if(index == slide_num) {
						jQuery(this).css('zIndex', '15');
						jQuery(this).fadeOut();
					}
					else jQuery(this).css('opacity', '0');
					if(slide_max > 1) // больше двух слайдов
					{
						num1 = slide_prev - 1;
						num2 = slide_prev + 1;
						if(num1 < 0) num1 = slide_max;
						if(num2 > slide_max) num2 = 0;
						var selector = _this.attr('id');
						if(selector == undefined)
							selector = '.' + _this.attr('class').replace(' ', '.');
						else
							selector = '#' + selector + '.' + _this.attr('class').replace(' ', '.');
						if(index == num1)
						{
							jQuery('#pseudo_before_style' + random_seed).remove();
							jQuery('head').append('<style id="pseudo_before_style' + random_seed + '" type="text/css"> ul' + selector + ':before { background: '+jQuery(this).css('backgroundImage')+'; } </style>');
						}
						if(index == num2)
						{
							jQuery('#pseudo_after_style' + random_seed).remove();
							jQuery('head').append('<style id="pseudo_after_style' + random_seed + '" type="text/css"> ul' + selector + ':after { background: '+jQuery(this).css('backgroundImage')+'; } </style>');
						}
					}
				});		
				_this.find("> li.deus_slide_nav, li.deus_slide_buttons").css('opacity', '1');
				_this.find("> li.deus_slide_nav, li.deus_slide_buttons").css('zIndex', '25');
				
				slide_num = slide_prev;
				if(slide_next == 0) slide_next = slide_max;
				else slide_next = slide_next - 1;

				slide_to_fade.fadeTo( "slow", 1 , function(){
					_this.find("> li.deus_slide_nav a").each(function(index){
						if(index == slide_num) jQuery(this).addClass('deus_slide_active');
						else jQuery(this).removeClass('deus_slide_active');
					})
				});
				if(options.onSlide !== undefined) options.onSlide({slideMax: slide_max, slideNum: slide_num});
			}
			
			/*CONSTRUCTOR*/
			_this.find("> li").each(function(index){
				if(slide_max < index)
				{
					slide_max = index;
					if((index > 0) && (jQuery(this).attr('class') != 'deus_slide_buttons')) _this.find("> li.deus_slide_nav").append('<a href="#" rel="'+(index - 1)+'"></a>');
				}
				if(index == 0) jQuery(this).css('zIndex', '20');
				else jQuery(this).css('opacity', '0');
			});
			_this.find("> li.deus_slide_nav").css('opacity', '1');//Навигацию и кнопки не надо прятать
			_this.find("> li.deus_slide_buttons").css('opacity', '1');

			if (_this.find("> li.deus_slide_nav").length) //Если есть навигация, то не считаем li навигации за слайд
				slide_max = slide_max - 1;

			if (_this.find("> li.deus_slide_buttons").length) //Если есть кнопки, то не считаем li кнопок за слайд
			{
				slide_max = slide_max - 1;
			}

			if(slide_max >= 1) //Два и более слайдов
			{
				_this.find('li.deus_slide_buttons').append('<a href="#" class="deus_slide_buttons_left"></a><a href="#" class="deus_slide_buttons_right"></a>');
				_this.find("> li:first-child").css('zIndex', '21');
				_this.find("> li.deus_slide_nav a").click(function(){
					var id = (jQuery(this).attr('rel')) * 1;
					_this.get(0).to_slide(id);
					return false;
				});
				
				_this.find("> li.deus_slide_nav a:first").addClass('deus_slide_active');
				
				slideTimer = setInterval( _this.get(0).next_slide, interval );
			}
			else _this.find("> li.deus_slide_nav").hide();

			if(slide_max > 1)//У нас есть больше двух слайдов
			{
				var selector = _this.attr('id');
				if(selector == undefined)
					selector = '.' + _this.attr('class').replace(' ', '.');
				else
					selector = '#' + selector + '.' + _this.attr('class').replace(' ', '.');

				_this.find("> li").each(function(index){
					if(index == slide_max)
						jQuery('head').append('<style id="pseudo_before_style' + random_seed + '" type="text/css"> ul' + selector + ':before { background: '+jQuery(this).css('backgroundImage')+'; } </style>');
					if(index == 1)
						jQuery('head').append('<style id="pseudo_after_style' + random_seed + '" type="text/css"> ul' + selector + ':after { background: '+jQuery(this).css('backgroundImage')+'; } </style>');
				});
			}

			_this.find('.deus_slide_buttons a.deus_slide_buttons_right').click(function(){
					$(_this).deusSlider('next');
					return false;
				});

			_this.find('.deus_slide_buttons a.deus_slide_buttons_left').click(function(){
					$(_this).deusSlider('prev');
					return false;
			});
			return this;
		},
		next: function() {
			this.get(0).next_slide();
			return this;
		},
		prev: function() {
			this.get(0).prev_slide();
			return this;
		},
		to: function(args) {
			if(!jQuery.isNumeric(args)) return false;
			console.log(args)
			this.get(0).to_slide(args);
			return this;
		}
	}

	jQuery.fn.deusSlider = function(methodOrOptions) {
		if ( deusSliderMethods[methodOrOptions] ) {
			return deusSliderMethods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof methodOrOptions === 'object' || typeof methodOrOptions === 'number' || ! methodOrOptions ) {
			// Default to "init"
			return deusSliderMethods.init.apply( this, arguments );
		} else {
			jQuery.error( 'Method ' +  methodOrOptions + '(' + typeof methodOrOptions + ') does not exist on jQuery.tooltip' );
		}
	}